const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})

// GET multiply endpoint - http://127.0.0.1:3000/multiply?a=3&b=5
app.get('/multiply', (req, res) => {
    try {
        const multiplicant = parseFloat(req.query.a);
        const multiplier = parseFloat(req.query.b);
        if (isNaN(multiplicant)) throw new Error('Invalid multiplicant value');
        if (isNaN(multiplier)) throw new Error('Invalid multiplier value');
        console.log({ multiplicant, multiplier });
        const product = multiply(multiplicant, multiplier);
        res.send(product.toString(10));
    } catch (err) {
        console.error(err.message);
        res.send("Couldn't calculate the product. Try again.");
    }
})

/**
 * This function multiplies two numbers.
 * @param {number} multiplicant
 * @param {number} multiplier
 * @returns {number} product
 */
const multiply = (multiplicant, multiplier) => {
    return multiplicant * multiplier;
}