# Node task

The project contains REST API with two following endpoints:
- Welcome endpoint
- Multiply endpoint

## Installation steps

1. Clone the repository

```sh
git clone <repo_url>
```

2. Install dependencies.

```sh
npm i
```

3. Start REST API service:
`node app.js`

Or in the latest NodeJS version(>=20):
`node --watch app.js`